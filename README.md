# Hook-CI

*Automated DevOps*

### Installation

Pull the hook-ci somewhere where you keep your tools, scripts, apps for development, e.g. `$HOME/dev/hook-ci`:

```
cd $HOME/dev
git clone https://gitlab.com/l.levickas/hook-ci.git hook-ci
cd hook-ci/pkg
composer install
yarn
git config --global --add core.hookspath /home/[YOUR_USER]/dev/hook-ci/hooks
```

**IMPORTANT! Change [YOUR_USER] to the actual username of your system**

### Editing hooks

Hooks can be edited to taste in `hooks/` directory.

### Example Hook

```
#!/bin/sh

HOOKCI_DIR=$(readlink -m $(git config -l | grep "core.hookspath" | cut -d'=' -f2)/..)

source "$HOOKCI_DIR/scripts/setpathvars.sh"

echo "GIT_HOOKS_RELPATH: $GIT_HOOKS_RELPATH"
echo "ROOT_DIR: $ROOT_DIR"
echo "SCRIPTS_DIR: $SCRIPTS_DIR"
echo "PKG_DIR: $PKG_DIR"
echo "HOOKS_DIR: $HOOKS_DIR"
echo "SUPER_DIR: $SUPER_DIR"
echo "REPO_DIR: $REPO_DIR"
```
