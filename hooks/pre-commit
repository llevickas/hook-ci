#! /bin/bash

set -e

HERE=$(pwd)
HOOKCI_DIR=$(readlink -m $(git config -l | grep "core.hookspath" | cut -d'=' -f2)/..)

source "$HOOKCI_DIR/scripts/setpathvars.sh"

if [[ ${HOOK_CI_SKIP} == "y"* ]]; then
	exit 0;
fi

function onERR {
	echo "[FAIL]"
	echo "$@"
	$@
	exit 1
}

function runLintOnFile {
	banner="$1";
	cmd="$2";
	files="$3";

	if [[ ! -z $files ]]; then
		echo -n "$banner";

		for file in $(echo $files); do
			cd $HERE && cd $(dirname $file)
			currentCommand=$(echo "$cmd" | sed -e "s#__FILE__#$SUPER_DIR/$file#")
			trap "onERR $currentCommand" ERR
			ultimateSuppress=$($currentCommand 2>/dev/null)
		done;

		cd $HERE && git add $files;
		echo "[OK]";
	fi
}

phpFiles=$(git --no-pager diff --cached --name-only --diff-filter=ACMR -- '***.php')
codeceptions=$(git ls-files codeception.yml **/codeception.yml)
phpunit=$(git ls-files  phpunit.xml tests/*.php tests/**/*.php)

runLintOnFile 'PHP Validity Check	' 'php -l __FILE__' "$phpFiles"
runLintOnFile 'PHP Code Style Check	' "php $PKG_DIR/vendor/bin/php-cs-fixer fix -q --rules=@PSR2,@Symfony --cache-file=/tmp/php-cs-fixes --show-progress=estimating __FILE__" "$phpFiles"

if [[ ! -z "$phpFiles" ]]; then
	if [[ ! -z "$codeceptions" ]];then
		runLintOnFile 'PHP Codeception Tests	' "php $PKG_DIR/vendor/bin/codecept -f --config __FILE__ run" "$codeceptions"
	elif [[ ! -z "$phpunit" ]]; then
		runLintOnFile 'PHP PHPUnit Tests	' "php vendor/bin/phpunit --stop-on-defect"
	fi
fi

cssFiles=$(git --no-pager diff --cached --name-only --diff-filter=ACMR -- '***.css ***.scss')
htmlFiles=$(git --no-pager diff --cached --name-only --diff-filter=ACMR -- '***.html')

runLintOnFile 'CSS Lint Check 		' "yarn exec $PKG_DIR/node_modules/.bin/csslint __FILE__" "$cssFiles"
runLintOnFile 'HTML Lint Check			' "$PKG_DIR/node_modules/.bin/prettier --write --single-quote --print-width 2000 __FILE__" "$htmlFiles"
