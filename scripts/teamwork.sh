#! /bin/sh

function getAuthToken() {
	API_TOKEN="$1"
	USER_PASSWORD="$2"
	USABLE_TOKEN=$(echo "$API_TOKEN:$USER_PASSWORD" | base64)

	echo ${USABLE_TOKEN::-4}
}

function api() {
	AUTH_HEADER="Authorization: Basic $TW_API_TOKEN"
	METHOD="$(echo $1 | awk '{print toupper($0)}')"
	URI="$2"
	DATA="$3"

	if [ ! -z "$TW_LOG_API_CALLS" ]; then
		echo "$(cat <<EOF
Requesting the TeamWork API:
METHOD: $METHOD
URI: $URI
DATA: $DATA
EOF
)"
	fi

	curl -s -X "$METHOD" -H "$AUTH_HEADER" -H "Content-Type: application/json" -d "$DATA" $TW_HOST$URI
}

function setTaskTags() {
	TASK_ID="$1"
	TAGS="$2"
	BODY="$(cat <<EOF
{
	"todo-item":{
		"tags":"$TAGS"
	}
}
EOF
)"

api 'PUT' "/tasks/$TASK_ID.json" "$BODY"
}

function addTaskTags() {
	TASK_ID="$1"
	TAGS="$2"
	BODY="$(cat <<EOF
{
	"replaceExistingTags":"false",
	"tags":{
		"content":"$TAGS"
	}
}
EOF
)"
api 'PUT' "/tasks/$TASK_ID/tags.json" "$BODY"
}

function addTaskComment() {
	TASK_ID="$1"
	COMMENT="$(echo "$2" | sed 's/"/\\"/g' | sed 's/$/\\n/g' | tr -d '\n')"
	BODY="$(cat <<EOF
{
	"comment": {
		"body": "$COMMENT",
		"notify": "",
		"isprivate": false,
		"pendingFileAttachments": "",
		"content-type": "TEXT"
	}
}
EOF
)"
	api 'POST' "/tasks/$TASK_ID/comments.json" "$BODY"
}

# Date must be in 2020-02-09T04:44:32+00:00 format, from:
# date -d @$(git show -s --format=%ct) --iso-8601=seconds -u
function addTimeLog() {
	TASK_ID="$1"
	DATETIME="$2"
	DESC="$(echo "$3" | sed 's/"/\\"/g' | sed 's/$/\\n/g' | tr -d '\n')"
	HOURS="${4:-1}"
	MINUTES="${5:-0}"
	BILLABLE="${6-:1}"
	DATE=$(echo $DATETIME | cut -f '1' -d 'T' | sed 's/-//g')
	TIME=$(echo $DATETIME | cut -f '2' -d 'T' | cut -f '1,2' -d ':')

	BODY="$(cat <<EOF
{
	"time-entry": {
		"description": "$DESC",
		"date": "$DATE",
		"time": "$TIME",
		"hours": "$HOURS",
		"minutes": "$MINUTES",
		"isbillable": "$BILLABLE"
	}
}
EOF
)"

	api 'POST' "/tasks/$TASK_ID/time_entries.json" "$BODY"
}

# Post a chat message via incoming hook URL
function postIncomingHookChatMessage() {
	INCOMING_HOOK_URL="$1"
	MESSAGE="$2"
	BODY="$(cat <<EOF
{
	"body": "$MESSAGE"
}
EOF
)"

	curl -s -X "POST" -H "Content-Type: application/json" -d "$BODY" "$INCOMING_HOOK_URL"
}

function getActiveTimers() {
	api 'GET' "/timers.json"
}

function getTask() {
	TASK_ID="$1"
	api 'GET' "/tasks/$TASK_ID.json"
}

function getTaskComments() {
	TASK_ID="$1"
	api 'GET' "/tasks/$TASK_ID/comments.json" | jq -r '.comments[] | {"author-firstname", body}' | le❯ teamwork.sh api GET '/tasks/25871135/comments.json' | jq -r '.comments[] | {"author-firstname", body}' | less
}

function readTask() {
	TASK_ID="$1"
	ATTR="$2"
	api 'GET' "/tasks/$TASK_ID.json" | jq -rj ".\"todo-item\"" | jq -r '(. | [.boardColumn.name, ."project-name", .content] | join("\t|\t")), .description | @sh' | sed "s/'//g" | less
}

function readTaskComments() {
	TASK_ID="$1"
	api GET "/tasks/$TASK_ID/comments.json" | jq -r '.comments[] | {datetime, author: [."author-firstname", ."author-lastname"] | join(" "), body} | [.datetime, .author, .body, "\n---"] | @sh' | sed "s/' '/\n/g" | sed "s/'//g"
}

"$@"
