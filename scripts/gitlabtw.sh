#! /bin/sh

tw="$(dirname $0)/teamwork.sh"
TW_TASK_CHECKED="false"
TW_PERSON_ID=""

# Get Teamwork Person ID from commiters email
function getPersonID() {
	if [ -z "$TW_PERSON_ID" ]; then
		export TW_PERSON_ID=$($tw 'api' 'GET' '/people.json' | jq --raw-output ".people[] | select(.[\"email-address\"]==\"$GITLAB_USER_EMAIL\") | .id")

		if [ -z "$TW_PERSON_ID" ]; then
			>&2 echo "Person with email: $GITLAB_USER_EMAIL not found. Probably wrong git config user.email!"
			exit 3
		fi
	fi

	echo $TW_PERSON_ID;
}

# Some branches may not be checkeed at all
function skipOnRefNames() {
	echo "
Skipping if ref name is skippable..."
	if [ ! -z "$TW_SKIP_REF_NAMES" ]; then
		if [ $(expr match "$CI_COMMIT_REF_NAME" "$TW_SKIP_REF_NAMES") -gt 0 ]; then
			exit 0
		fi
	fi
}

# Ensure branch name is in required format ^(feature|fix|test)-TASK_ID-short-title$
function getTaskID() {
	ID=$(echo $CI_COMMIT_REF_NAME | cut -f 2 -d '-')

	if [ $(expr match "$ID" '[0-9]\+$') -eq 0 ]; then
		>&2 echo "CI_COMMIT_REF_NAME: $CI_COMMIT_REF_NAME was pushed that does not meet the branch naming requirements."
		exit 1
	fi

	echo $ID
}

# Warn about branches that don't have a TeamWork task
function checkTaskExists() {
	if [ "$TW_TASK_CHECKED" != "true" ]; then
		echo "
Checking if Task exists in TeamWork..."
		ID="$1"
		TASK_RESPONSE="$("$tw" api 'GET' "/tasks/$ID.json")"

		if [ $(expr match "$TASK_RESPONSE" '.*"STATUS":"OK".*') -eq 0 ];then
			>&2 echo "Teamwork Task with ID: $ID does not exist. Origin branch of error: $CI_COMMIT_REF_NAME"
			exit 2
		else
			echo "Teamwork Task with ID: $ID exist. OK."
		fi

		export TW_TASK_CHECKED="true"
	fi
}

# Comment TeamWork Task with git log content
# If log contains a line with:
# COMMENT 1
function commentTaskWithLog() {
	ID=$(getTaskID)
	checkTaskExists $ID
	if [ -n "$CI_COMMIT_DESCRIPTION" ] && [ $(expr match "$CI_COMMIT_DESCRIPTION" '.*COMMENT 1.*') -gt 0 ]; then
		echo "
Commenting the TeamWork task with commit message..."
		$tw addTaskComment "$ID" "$(echo "$CI_COMMIT_DESCRIPTION" | sed 's/\(HOURS [0-9]\+\)\|\(MINUTES [0-9]\+\)\|\(BILLABLE [0-1]\)\|\(COMMENT 1\)//g')"
	fi
}

# Log TeamWork Task
# Must include these lines to work:
# HOURS [0-9]+
# MINUTES [0-9]+
# BILLABLE [0-1]
function logTime() {
	ID=$(getTaskID)
	checkTaskExists $ID
	HOURS="$(echo "$CI_COMMIT_DESCRIPTION" | grep 'HOURS [0-9]\+$' | grep -o '[0-9]\+')"
	MINUTES="$(echo "$CI_COMMIT_DESCRIPTION" | grep 'MINUTES [0-9]\+$' | grep -o '[0-9]\+')"
	BILLABLE="$(echo "$CI_COMMIT_DESCRIPTION" | grep 'BILLABLE [0-1]\+$' | grep -o '[0-1]')"
	MESSAGE="$(echo "$CI_COMMIT_DESCRIPTION" | sed 's/\(HOURS [0-9]\+\)\|\(MINUTES [0-9]\+\)\|\(BILLABLE [0-1]\)\|\(COMMENT 1\)//g')"

	if [ -n "$CI_COMMIT_DESCRIPTION" ] && [ -n "$HOURS" ]  && [ -n "$MINUTES" ] && [ -n "$BILLABLE" ]; then
		echo "
Logging time from commit message..."
		$tw addTimeLog \
			"$ID" \
			"$(date -d @$(git show -s --format=%ct) -Iseconds -u)" \
			"$MESSAGE" \
			"$HOURS" \
			"$MINUTES" \
			"$BILLABLE"
	else
		echo "
Commit message hasn't all fields: HOURS MINUTES BILLABLE in description. Skipping..."
	fi
}

function tagTask() {
	ID=$(getTaskID)
	checkTaskExists $ID
	TAGS="GitBranchOK"
	echo "
Setting tags for the TeamWork task..."
	$tw addTaskTags "$ID" "$TAGS"
}

function everything() {
  skipOnRefNames
  commentTaskWithLog
  tagTask
  logTime
}

"$@"
