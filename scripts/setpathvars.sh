#! /bin/sh

CWD=$(pwd)
SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ]; do
  TARGET="$(readlink "$SOURCE")"
  if [[ $SOURCE == /* ]]; then
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    SOURCE="$DIR/$TARGET"
  fi
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

export GIT_HOOKS_RELPATH="$(git config core.hookspath)"
export ROOT_DIR=$(readlink -m "$DIR/..")
export SCRIPTS_DIR="$ROOT_DIR/scripts"
export PKG_DIR="$ROOT_DIR/pkg"
export HOOKS_DIR="$ROOT_DIR/hooks"
export SUPER_DIR=$(git rev-parse --show-toplevel)

cd $ROOT_DIR;
export REPO_DIR=$(git rev-parse --show-toplevel)
cd $CWD
